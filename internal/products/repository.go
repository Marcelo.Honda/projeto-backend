package products

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)


type ProductRepository struct {
	collection *mongo.Collection
}

func NewProductRepository (client *mongo.Client, databaseName, collectionName string) *ProductRepository {
	collection := client.Database("produtos-do-sistema").Collection("produto")

	return &ProductRepository{collection: collection}
}

func (repos *ProductRepository) Insert(product *Product) error {
	_, err := repos.collection.InsertOn(context.TODO(), product)
	return err
}

func (repos *ProductRepository) GetAll() ([]Product, error) {
	var products []Product
	
	ctx, cancel := context.WithTimeout(context.Background(), 30 * time.Second)
	defer cancel()
	
	cursor, err := repos.collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	
	for cursor.Next(ctx) {
		var product Product
		if err := cursor.Decode(&product); err != nil {
			return nil, err
		}
		
		products = append(products, product)
	}
	
	if err := cursor.Err(); err != nil {
		return nil, err
	}
	
	return products, nil
}

