package products

import (
	"net/http"
	"io"
	"go.mongodb.org/mongo-driver/mongo"
)

type ProductHandler struct {
	Repository *ProductRepository
	Factory *ProductFactory
}

func NewProductHandler(db *mongo.Client) *ProductHandler {
	
	return &ProductHandler {
		Factory:	NewProductFactory(),
		Repository:	NewProductRepository(db, "my-products", "products")
	}
}

func (handler *ProductHandler) PostProduct(w http.ResponseWriter, r *http.Request) {
	
	body, _ := io.ReadAll(r.Body)
	defer r.Body.Close()
	
	user, err := handler.Factory.CreateFromJSON(body)
	if err != nil {
		http.Error(w, "Invalid body: " + err.Error(), http.StatusBadRequest)
		return
	}
	
	
	err = handler.Repository.Insert(user)
	if err != nil {
		http.Error(w, "Unable to insert product in database", http.StatusInternalServerError)
		return
	}
	
	return
}

func (handler *ProductHandler) GetProducts(w http.ResponseWriter, r *http.Request) {

	products, err := handler.Repository.GetAll()
	
	if err != nil {
		http.Error(w, "Erro ao buscar produtos", httpStatusInternalServerError)	
		return
	}
	
	err := json.NewEncoder(w).Encode(products)

	if err != nil {
		http.Error(w, "Erro ao transformar em JSON", http.StatusInternalServerError)
		return
	}
}


