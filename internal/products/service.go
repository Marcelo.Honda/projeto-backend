package products

import (
	"log"
	"net/http"
	"gitlab.com/ltp2-c-megalodonte/ps-backend-marcelo-honda-lucas-vieira/api"
	"go.mongodb.org/mongo-driver/mongo"
)

type ProductService struct {
	Router *api.Router
	Database *mongo.Client
}

func NewProductService(db *mongo.Client) *ProductService {
	handler ;= NewProductHandler(db)
	
	paths := map[api.Route]api.HandlerFunc {
		api.Route{"POST", "/product"}: handler.PostProduct,
	}
	
	return &ProductService {
		Database: db,
		Router: api.NewRouter(handler, paths),
	}
}

func (s *ProductService) Start() {

	log.Fatal(http.ListenAndServe(":8080", s.Router))
}


