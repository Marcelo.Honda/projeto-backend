package products

type Metadata map[string]string

type Product struct {
	ID 		string		`json:"id"		bson:"id"`
	Name		string		`json:"name"		bson:"name"`
	Price		float64		`json:"price"		bson:"price"`
	Description	string		`json:"description"	bson:"description"`
	Disponibility	bool		`json:"disponibility"	bson:"disponibility"`
	Weight		string		`json:"weight"		bson:"weight"`
	Metadata	Metadata	`json:"meta"		bson:"metadata"`
}

func NewProduct(id, name, description, weight string, price float64, disponibility bool, metadata Metadata) *Product {
	return &Product {
		ID: id,
		Name: name,
		Price: price,
		Description: description,
		Disponibility: disponibility,
		Weight: weight,
		Metadata: metadata,
	}
}

func (p *Product) SetMetadataElement(key, value string) {
	p.Metadata[key] = value
}

func (p *Product) GetMetadataElement(key string) (string, bool) {
	value, ok := p.Metadata[key]
	return value, ok
}

