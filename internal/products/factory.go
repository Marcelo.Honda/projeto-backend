package products

import (
	"gitlab.com/ltp2-c-megalodonte/ps-backend-marcelo-honda-lucas-vieira/internal/utility"
	"encoding/json"
	"regexp"
	"errors"
	"strings"
	"fmt"
	"strconv"
)

type ProductFactory struct {}

func NewProductFactory() *ProductFactory {
	return &ProductFactory{}
}

func (f *ProductFactory) CreateFromJSON(data []byte) (*Product, error) {
	var product Product
	
	if err := json.Unmarshal(data, &product); err != nil {
		return nil, err
	}
	
	return f.Validate(&product)
}


func (f *ProductFactory) Validate(product *Product) (*Product, error) {

	if err := f.validateName(product); err != nil {
		return product, err
	}
	
	if err := f.validateDescription(product); err != nil {
		return product, err
	}

	if err := f.validatePrice(product); err != nil {
		return product, err
	}
	
	if err := f.validateWeight(product); err != nil {
		return product, err
	}
	
	f.ensureID(product)
	f.ensureMetadata(product)
	
	return product, nil
}

func (f *ProductFactory) ensureID(product *Product) {
	if product.ID == "" {
		product.ID = utility.GenerateID()
		return
	}
	
	if !utility.ValidateID(product.Id) {
		product.ID = utility.GenerateID()
		return
	}

	return
}

func (f *ProductFactory) ensureMetadata(product *Product) {
	if product.Metadata == nil {
		product.Metadata = make(map[string]string)
		return
	}

	if len(product.Metadata) > 20 {
		product.Metadata = make(map[string]string)
		return
	}

	for key, value := range(product.Metadata) {
		if (len(value) > 512) {
			delete(user.Metadata, key)
		}
	}
	
	return
}

func (f *ProductFactory) validateName(product *Product) {
	if product.Name == "" {
		return errors.New("invalid name: cannot be empty")
	}
	
	return nil
}

func (f *ProductFactory) validateDescription(product *Product) {
	if len(product.Description) > 512 {
		product.Description = product.Description[:512]
	}

	return
}

func (f *ProductFactory) validatePrice(product *Product) {
	precoFormatado := fmt.Sprintf("%.2f", product.Price)
	
	productPrice, _ := strconv.ParseFloat(precoFormatado, 64)
	return
}

func (f *ProductFactory) validateWeight(product *Product) {
	if !strings.HasSuffix(product.Weight, "kg") && !strings.HasSuffix(product.Weight, "g") {
		weight, err := strconv.Atoi(product.Weight)
		
		if err != nil {
			return errors.New("invalid weight: must be numbers")
		}
		
		var unit string
		if weight > 20 {
			unit = "g"
		} else {
			unit = "kg"
		}
		
		product.Weight += unit
	}
	pesoSemUnidade := regexp.MustCompile("[gGkK][gG]?").ReplaceAllString(product.Weight, "")
	weight, err := strconv.Atoi(pesoSemUnidade)
	
	if err != nil {
		return errors.New("invalid weight: must be numbers")
	}
	
	return
}



























