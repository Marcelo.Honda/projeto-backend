package main

import (
	"gitlab.com/ltp2-c-megalodonte/ps-backend-marcelo-honda-lucas-vieira/internal/users"
	"gitlab.com/ltp2-c-megalodonte/ps-backend-marcelo-honda-lucas-vieira/internal/database"
	
	"log"
)

func main() {
	
	db := &database.MongoClient{}
	
	err := db.Start()
	if err != nil {
		log.Fatal("[FATAL] Could not connect with the database")
	}
	
	products.NewProductService(db.GetClient()).Start()
}
